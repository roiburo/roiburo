import Vue from 'vue'
import App from './app.vue'
import Vue2TouchEvents from 'vue2-touch-events'
import { ObserveVisibility } from 'vue-observe-visibility'
import Vuelidate from 'vuelidate'
import VueMask from 'v-mask'
import { directive as onClickaway } from 'vue-clickaway';
import './scss/reset.scss'
import './scss/transitions.scss'




Vue.use(VueMask);
Vue.use(Vuelidate); 
Vue.use(Vue2TouchEvents);

Vue.directive('observe-visibility', ObserveVisibility);
Vue.directive('on-clickaway', onClickaway);

new Vue({
    el: '#app',
    render: h => h(App)
})